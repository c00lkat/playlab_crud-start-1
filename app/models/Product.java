package models;

import java.util.*;
import javax.persistence.*;
import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

@Entity
public class Product extends Model {
 //Properties
 @Id //annotate primary key field
 private Long id;

 @Constraints.Required
 private String name;

 @Constraints.Required
 private String description;

 @Constraints.Required
 private int stock;

 @Constraints.Required 
 private double price;

 //Default constructor
 public Product() {
 }

 //generic query helper entity Computer with id long
 public static Finder<Long, Product> find = new Finder<Long, Product>(Product.class);

	//Find all products in the database
	//Filter product name
 public static List<Product> findAll() {
	return Product.find.all();
}	


 //Constructor to initialize object
 public Product(Long id, String name, String description, int stock, double price){
	this.id = id;
	this.name = name;
	this.description = description;
	this.stock = stock;
	this.price = price;
}

	//Id
	 public Long getId(){
		return id;
	}
	 public void setId(Long id){
		this.id = id;
	}	

	//name
	 public String getName(){
		return name;
	}
	 public void setName(String name){
		this.name = name;
	}

	//Description
	 public String getDescription(){
		return description;
	}
	 public void setDescription(String description){
		this.description = description;
	}

	//Stock
	 public int getStock(){
		return stock;
	}
	 public void setStock(int stock){
		this.stock = stock;
	}

	//Price
	 public double getPrice(){
		return price;
	}
	 public void setPrice(double price){
		this.price = price;
	}



}
